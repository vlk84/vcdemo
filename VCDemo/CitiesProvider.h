//
//  CitiesProvider.h
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CitiesProvider : NSObject

+ (CitiesProvider*) defaultCitiesProvider;

- (NSArray*) cities;
- (NSArray*) citiesWithCountryName:(NSString*)country;

@end
