//
//  CityCell+VCDemo.h
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "CityCell.h"

@interface CityCell (VCDemo)

- (void) setupWithCity:(NSDictionary*)data atIndex:(int)index backgroundColor:(UIColor*)inBackgroundColor;

@end
