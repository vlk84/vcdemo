//
//  USCitiesViewController.h
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/8/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface USCitiesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
