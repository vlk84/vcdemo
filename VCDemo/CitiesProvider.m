//
//  CitiesProvider.m
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "CitiesProvider.h"

@implementation CitiesProvider{
    NSArray *_cities;
}

#pragma mark - Data

- (void) loadCityData{
    NSError *error;
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"cities" withExtension:@"json"]];
    _cities = [NSJSONSerialization JSONObjectWithData: data
                                              options: NSJSONReadingAllowFragments
                                                error: &error] ;
}

- (NSArray*) cities{
    return [NSArray arrayWithArray:_cities];
}

- (NSArray*) citiesWithCountryName:(NSString*)country{
    if (!country) {
        return [self cities];
    }
    
    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL(NSDictionary* evaluatedObject, NSDictionary *bindings) {
        return [evaluatedObject[@"country"] isEqualToString:country];
    }];
    
    return [_cities filteredArrayUsingPredicate:filter];
}


#pragma mark - Life-cycle

+ (CitiesProvider*) defaultCitiesProvider{
    static dispatch_once_t onceToken;
    static CitiesProvider *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CitiesProvider alloc] init];
    });
    
    return instance;
}

- (id) init{
    self = [super init];

    if (self){
        [self loadCityData];
    }
    
    return self;
}

@end
