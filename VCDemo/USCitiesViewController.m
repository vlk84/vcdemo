//
//  USCitiesViewController.m
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/8/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "USCitiesViewController.h"
#import "CityCell.h"
#import "CityCell+VCDemo.h"
#import "CitiesProvider.h"
#import "ArrayTableViewDataSource.h"


@interface USCitiesViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation USCitiesViewController{
    ArrayTableViewDataSource *dataSource;
}

#pragma mark - Controller

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"US";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CityCell" bundle:nil] forCellReuseIdentifier:@"CityCell"];
    self.tableView.rowHeight = 50.0f;

    ConfiguringCellBlock block = ^(CityCell *cell, NSDictionary *data, NSIndexPath *indexPath) {
        [cell setupWithCity:data
                    atIndex:indexPath.row+1
            backgroundColor:indexPath.row % 2 ? [UIColor lightGrayColor] : [UIColor whiteColor]];
    };
    
    dataSource = [[ArrayTableViewDataSource alloc] initWithDataArray:[[CitiesProvider defaultCitiesProvider] citiesWithCountryName:@"US"]
                                                      cellIdentifier:@"CityCell"
                                                           cellBlock:block];
    self.tableView.dataSource = dataSource;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
