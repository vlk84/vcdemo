//
//  AppDelegate.h
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/8/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CitiesViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *viewController;

@end
