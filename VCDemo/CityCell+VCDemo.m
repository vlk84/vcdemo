//
//  CityCell+VCDemo.m
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "CityCell+VCDemo.h"

@implementation CityCell (VCDemo)

- (void) setupWithCity:(NSDictionary*)data atIndex:(int)index backgroundColor:(UIColor*)inBackgroundColor{
    self.cityLabel.text = data[@"name"];
    self.countryLabel.text = data[@"country"];
    self.populationLabel.text = [data[@"population"] stringValue];
    
    self.orderLabel.text = [NSString stringWithFormat:@"%d", index];
    
    self.contentView.backgroundColor = inBackgroundColor;
    [[self.contentView subviews] setValue:inBackgroundColor forKey:@"backgroundColor"];
}

@end
