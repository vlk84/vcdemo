//
//  ArrayTableViewDataSource.h
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ConfiguringCellBlock)(id cell, id data, NSIndexPath *indexPath);



@interface ArrayTableViewDataSource : NSObject <UITableViewDataSource>

- (id) dataAtIndexPath:(NSIndexPath*)indexPath;

- (id) initWithDataArray:(NSArray*)inData cellIdentifier:(NSString*)cellID cellBlock:(ConfiguringCellBlock)block;

@end
