//
//  CityCell.m
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "CityCell.h"

@implementation CityCell


- (void) prepareForReuse{
    [super prepareForReuse];
    
    self.orderLabel.text = nil;
    self.populationLabel.text = nil;
    self.cityLabel.text = nil;
    self.countryLabel.text = nil;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
