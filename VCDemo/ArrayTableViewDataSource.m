//
//  ArrayTableViewDataSource.m
//  VCDemo
//
//  Created by Vladimir Kolbun on 7/15/13.
//  Copyright (c) 2013 Vladimir Kolbun. All rights reserved.
//

#import "ArrayTableViewDataSource.h"

@implementation ArrayTableViewDataSource{
    NSArray                 *_data;
    NSString                *_cellIdentifier;
    ConfiguringCellBlock    _configuringBlock;
}

#pragma mark - UITableViewDataSource

- (int) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id cell = [tableView dequeueReusableCellWithIdentifier:_cellIdentifier];
    
    if (_configuringBlock){
        _configuringBlock(cell, [self dataAtIndexPath:indexPath], indexPath);
    }
    
    return cell;
}

- (id) dataAtIndexPath:(NSIndexPath*)indexPath{
    return _data[indexPath.row];
}

#pragma mark - Life-cycle

- (id) initWithDataArray:(NSArray*)inData cellIdentifier:(NSString*)cellID cellBlock:(ConfiguringCellBlock)block{
    self = [super init];
    
    if (self){
        _data = [inData copy];
        _cellIdentifier = [cellID copy];
        _configuringBlock = [block copy];
    }
    
    return self;
}

@end
